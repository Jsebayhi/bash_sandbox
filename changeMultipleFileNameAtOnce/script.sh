
if [ "$1" = "-help" ]; then
	echo "[directory targetted][filesContainingWord][substringToReplace][by]"
	exit
fi;
find $1 -type f -name $2*  | while read -r FILE ; do newfile=$(echo ${FILE} | sed -e 's/'$3'/'$4'/');echo "moving ${FILE} as $newfile";mv "${FILE}" "${newfile}"; done;ls -R $1 